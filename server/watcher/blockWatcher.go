package watcher

import (
	"errors"
	"ethereum/server/conf"
	"ethereum/server/db"
	"ethereum/server/helper"
	"log"
	"sync"
	"time"

	"github.com/ybbus/jsonrpc"
)

var accounts = make(map[string]bool)

var errTypeAssertion = errors.New("Type assertion error")

var lastBlock struct {
	BlockNumber string
	sync.RWMutex
}

//Start watchers
func Start() {
	getSetAccounts()

	go updateLastBlock()
	time.Sleep(2 * time.Second)
	go updateTxs()
	go watchBalance()
	go watchTxsConfirmations()
}

func watchTxsConfirmations() {
	for {
		txs, err := db.GetUnconfirmedTransactions()
		if err != nil {
			log.Println(err)
		}

		for _, tx := range txs {
			err = updateConfirmations(tx)
			if err != nil {
				log.Println(err)
			}
		}

		time.Sleep(20 * time.Second)
	}
}

func updateConfirmations(tx conf.InputTransaction) error {
	number, err := helper.BlockHashToNumber(lastBlock.BlockNumber)
	if err != nil {
		return err
	}

	newNumber := number - tx.BlockNumber
	if tx.Confirmations == newNumber {
		return nil
	}
	tx.Confirmations = newNumber

	return db.UpdateTransaction(tx)
}

func getSetAccounts() {
	client := jsonrpc.NewRPCClient(conf.Endpoint)
	resp, err := client.Call("eth_accounts")
	if err != nil {
		log.Println(err)
	}

	if arr, ok := resp.Result.([]interface{}); ok {
		for _, v := range arr {
			if acc, ok := v.(string); ok {
				accounts[acc] = true
			}
		}
	}

	for k := range accounts {
		balance, err := getBalance(k)
		if err != nil {
			log.Println(err)
		}
		if err := db.AddAccount(conf.Account{Address: k, Balance: balance}); err != nil {
			log.Println(err)
		}
	}
}

func updateTxs() {
	for {
		processLastBlockTxs()
		time.Sleep(10 * time.Second)
	}
}

func processLastBlockTxs() []string {
	lastBlock.RWMutex.Lock()
	defer lastBlock.RWMutex.Unlock()
	resp, err := jsonrpc.NewRPCClient(conf.Endpoint).Call("eth_getBlockByNumber", lastBlock.BlockNumber, true)
	if err != nil {
		log.Println(err)
	}

	if v, ok := resp.Result.(map[string]interface{}); ok {
		if transactions, ok := v["transactions"]; ok {
			if arr, ok := transactions.([]interface{}); ok {
				for _, interf := range arr {
					if mp, ok := interf.(map[string]interface{}); ok {
						if to, ok := mp["to"].(string); ok {
							if _, ok := accounts[to]; ok {
								//Add transaction to DB
								tx, err := getTxObject(mp)
								if err != nil {
									log.Println(err)
								}
								accountID, err := db.GetAccountIDByAddress(to)
								if err != nil {
									log.Println(err)
								}
								tx.AccountID = accountID

								err = db.AddInputTransaction(tx)
								if err != nil {
									log.Println(err)
								}

								b, err := getBalance(to)
								if err != nil {
									log.Println(err)
								}
								if err = db.AddAccount(conf.Account{Address: to, Balance: b}); err != nil {
									log.Println(err)
								}
							}
						}
					}
				}
			}
		}
	}

	return nil
}

func getTxObject(res map[string]interface{}) (conf.InputTransaction, error) {
	var tx conf.InputTransaction
	if value, ok := res["value"].(string); ok {
		tx.Amount = value
	} else {
		return tx, errTypeAssertion
	}

	tx.Date = time.Now().Unix()
	tx.IsNew = true

	if hash, ok := res["hash"].(string); ok {
		tx.TxHash = hash
	}

	if blockNumber, ok := res["blockNumber"].(string); ok {
		lastBlockNumber, err := helper.BlockHashToNumber(lastBlock.BlockNumber)
		if err != nil {
			return tx, err
		}

		currentBlockNumber, err := helper.BlockHashToNumber(blockNumber)
		if err != nil {
			return tx, err
		}
		tx.BlockNumber = currentBlockNumber
		tx.Confirmations = lastBlockNumber - currentBlockNumber
	}

	return tx, nil
}

func updateLastBlock() {
	for {
		resp, err := jsonrpc.NewRPCClient(conf.Endpoint).Call("eth_blockNumber")
		if err != nil {
			log.Println(err)
		}

		if v, ok := resp.Result.(string); ok {
			if lastBlock.BlockNumber != v {
				lastBlock.RWMutex.Lock()
				lastBlock.BlockNumber = v
				lastBlock.RWMutex.Unlock()
			}
		}

		time.Sleep(8 * time.Second)
	}
}

func getBalance(address string) (string, error) {
	resp, err := jsonrpc.NewRPCClient(conf.Endpoint).Call("eth_getBalance", address, "latest")
	if err != nil {
		return "", err
	}
	if v, ok := resp.Result.(string); ok {
		return v, nil
	}

	return "", errors.New("No balance presented")
}

func watchBalance() {
	for {
		for k := range accounts {
			b, err := getBalance(k)
			if err != nil {
				log.Println(err)
			}
			dbBalance, err := db.GetBalanceByAccount(k)
			if err != nil {
				log.Println(err)
			}
			if b != dbBalance {
				if err = db.AddAccount(conf.Account{Address: k, Balance: b}); err != nil {
					log.Println(err)
				}
			}
		}

		time.Sleep(10 * time.Second)
	}
}
