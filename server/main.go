package main

import (
	"encoding/json"
	"errors"
	"ethereum/server/conf"
	"ethereum/server/db"
	"ethereum/server/helper"
	"ethereum/server/watcher"
	"fmt"
	"net"
	"time"

	"github.com/buger/jsonparser"
	"github.com/ybbus/jsonrpc"
)

type sendEthType struct {
	From     string `json:"from"`
	To       string `json:"to"`
	Gas      string `json:"gas"`
	GasPrice string `json:"gasPrice"`
	Value    string `json:"value"`
	Data     string `json:"data"`
}

type getLastResponse struct {
	Date          int64   `json:"date"`
	Address       string  `json:"address"`
	Amount        float64 `json:"amount"`
	Confirmations int     `json:"confirmations"`
}

type response struct {
	IsSuccess bool              `json:"isSuccess"`
	Result    []getLastResponse `json:"result,omitempty"`
	TxHash    string            `json:"hash,omitempty"`
	Error     string            `json:"error,omitempty"`
}

func init() {
	//Wait at least 40sec cause geth don't start syncing immediately after start
	time.Sleep(40 * time.Second)
	for {
		fmt.Println("Node is syncing, please wait...")
		ok, err := isSyncing()
		if err != nil {
			panic(err)
		}
		if !ok {
			fmt.Println("Sync done")
			break
		}
		time.Sleep(10 * time.Second)
	}
	//starting block watcher
	go watcher.Start()
}

func main() {
	listener, err := net.Listen("tcp", "server:3333")
	if err != nil {
		panic(err)
	}

	defer listener.Close()

	for {
		conn, err := listener.Accept()
		if err != nil {
			panic(err)
		}

		go handle(conn)
	}
}

func handle(conn net.Conn) {
	defer conn.Close()
	data := make([]byte, 512)

	n, err := conn.Read(data)
	if err != nil {
		conn.Write([]byte(err.Error()))
		return
	}
	data = data[:n]

	method, err := jsonparser.GetString(data, "method")
	if err != nil {
		conn.Write([]byte(err.Error()))
		return
	}

	switch method {
	case conf.SendEthMethod:
		hash, err := sendEth(data)
		if err != nil {
			conn.Write(buildResponse(false, "", err, nil))
			return
		}
		conn.Write(buildResponse(true, hash, nil, nil))
		return
	case conf.GetLastMethod:
		txs, err := getLast()
		if err != nil {
			conn.Write(buildResponse(false, "", err, nil))
			return
		}

		conn.Write(buildResponse(true, "", nil, txs))
	default:
		conn.Write(buildResponse(false, "", errors.New("Not supported method"), nil))
		return
	}

}

func sendEth(data []byte) (string, error) {
	from, err := jsonparser.GetString(data, "from")
	if err != nil {
		return "", err
	}
	to, err := jsonparser.GetString(data, "to")
	if err != nil {
		return "", err
	}
	amount, err := jsonparser.GetFloat(data, "amount")
	if err != nil {
		return "", err
	}

	if amount <= 0 || from == "" || to == "" {
		return "", errors.New("Input params are not valid")
	}

	client := jsonrpc.NewRPCClient(conf.Endpoint)
	value := helper.EthToWei(amount)
	resp, err := client.Call("eth_sendTransaction", sendEthType{From: from, To: to, Gas: conf.Gas, GasPrice: conf.GasPrice, Value: value})
	if err != nil {
		return "", err
	}

	hash, ok := resp.Result.(string)
	if !ok {
		return "", errors.New("Wrong resp")
	}
	accountID, err := db.GetAccountIDByAddress(from)
	if err != nil {
		return "", err
	}
	var tx = conf.OutputTransaction{AccountID: accountID, Amount: value, Date: time.Now().Unix(), To: to, TxHash: hash}

	err = db.AddOutputTransaction(tx)
	if err != nil {
		return "", err
	}

	return hash, nil
}

func getLast() ([]getLastResponse, error) {
	txs, err := db.GetLastTransactions()
	if err != nil {
		return nil, err
	}

	var resp []getLastResponse
	for _, tx := range txs {
		var t getLastResponse
		address, err := db.GetAccountAddressByID(tx.AccountID)
		if err != nil {
			return nil, err
		}
		amount, err := helper.WeiToEth(tx.Amount)
		if err != nil {
			return nil, err
		}
		t.Address = address
		t.Amount = amount
		t.Confirmations = tx.Confirmations
		t.Date = tx.Date

		resp = append(resp, t)

	}
	return resp, nil
}

func isSyncing() (bool, error) {
	resp, err := jsonrpc.NewRPCClient(conf.Endpoint).Call("eth_syncing")
	if err != nil {
		return true, err
	}

	if v, ok := resp.Result.(bool); ok {
		return v, nil
	}

	return true, nil
}

func buildResponse(isSuccess bool, hash string, err error, last []getLastResponse) []byte {
	var resp response
	resp.IsSuccess = true
	resp.Error = ""
	resp.Result = last
	resp.TxHash = hash
	if err != nil {
		resp.IsSuccess = false
		resp.Error = err.Error()
	}
	r, _ := json.Marshal(resp)
	return r
}
