BEGIN;

CREATE TABLE IF NOT EXISTS accounts (
    id SERIAL PRIMARY KEY,
    "address" VARCHAR(50) NOT NULL,
    "balance" VARCHAR(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS input_transactions (
    id SERIAL PRIMARY KEY,
    account_id INT NOT NULL,
    tx_hash VARCHAR(100) NOT NULL,
    amount VARCHAR(50) NOT NULL,
    block_number INT,
    "date" INT NOT NULL,
    confirmations INT NOT NULL,
    isNew BOOLEAN DEFAULT TRUE,
    
    FOREIGN KEY (account_id) REFERENCES accounts(id)
);

CREATE TABLE IF NOT EXISTS output_transactions (
    id SERIAL PRIMARY KEY,
    account_id INT NOT NULL,
    tx_hash VARCHAR(100) NOT NULL,
    "to" VARCHAR(50) NOT NULL,
    amount VARCHAR(50) NOT NULL,
    "date" INT NOT NULL,

    FOREIGN KEY (account_id) REFERENCES accounts(id)
);

CREATE UNIQUE INDEX IF NOT EXISTS accounts_address ON accounts("address");
CREATE UNIQUE INDEX IF NOT EXISTS input_transactions_tx_hash ON input_transactions(tx_hash);
CREATE UNIQUE INDEX IF NOT EXISTS output_transactions_tx_hash ON output_transactions(tx_hash);

COMMIT;