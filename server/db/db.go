package db

import (
	"database/sql"
	"ethereum/server/conf"
	"fmt"
	"io/ioutil"

	_ "github.com/lib/pq"
)

var ConnectionPool *sql.DB

func init() {
	db, err := sql.Open("postgres", fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=disable", "postgres", "root", "postgres:5432", "postgres"))
	if err != nil {
		panic(err)
	}

	db.SetMaxOpenConns(10)
	ConnectionPool = db

	res, err := ioutil.ReadFile("init.sql")
	if err != nil {
		panic(err)
	}

	_, err = db.Exec(string(res))
	if err != nil {
		panic(err)
	}
}

func AddAccount(account conf.Account) error {
	_, err := ConnectionPool.Exec("INSERT INTO accounts (address, balance) VALUES ($1, $2) ON CONFLICT (address) DO UPDATE SET balance = $3", account.Address, account.Balance, account.Balance)
	if err != nil {
		return err
	}

	return nil
}

func AddInputTransaction(tx conf.InputTransaction) error {
	_, err := ConnectionPool.Exec("INSERT INTO input_transactions (date, account_id, block_number, amount, confirmations, isNew, tx_hash) VALUES ($1, $2, $3, $4, $5, $6, $7) ON CONFLICT (tx_hash) DO NOTHING", tx.Date, tx.AccountID, tx.BlockNumber, tx.Amount, tx.Confirmations, true, tx.TxHash)
	return err
}

func AddOutputTransaction(tx conf.OutputTransaction) error {
	_, err := ConnectionPool.Exec(`INSERT INTO output_transactions (account_id, tx_hash, "to", amount, date) VALUES ($1, $2, $3, $4, $5) ON CONFLICT (tx_hash) DO NOTHING`, tx.AccountID, tx.TxHash, tx.To, tx.Amount, tx.Date)
	return err
}

func GetBalanceByAccount(address string) (string, error) {
	row := ConnectionPool.QueryRow("SELECT balance FROM accounts WHERE address = $1", address)

	var balance string
	if err := row.Scan(&balance); err != nil {
		return "", err
	}

	return balance, nil
}

func GetUnconfirmedTransactions() ([]conf.InputTransaction, error) {
	rows, err := ConnectionPool.Query("SELECT * FROM input_transactions WHERE confirmations < 6 ")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var result []conf.InputTransaction
	for rows.Next() {
		var tx conf.InputTransaction

		if err = rows.Scan(&tx.ID, &tx.AccountID, &tx.TxHash, &tx.Amount, &tx.BlockNumber, &tx.Date, &tx.Confirmations, &tx.IsNew); err != nil {
			return nil, err
		}

		result = append(result, tx)
	}

	return result, nil
}

func UpdateTransaction(tx conf.InputTransaction) error {
	_, err := ConnectionPool.Exec("UPDATE input_transactions SET confirmations = $1 WHERE id = $2", tx.Confirmations, tx.ID)
	if err != nil {
		return err
	}

	return nil
}

func GetLastTransactions() ([]conf.InputTransaction, error) {
	rows, err := ConnectionPool.Query("SELECT * FROM input_transactions WHERE confirmations < 3 OR isNew = true")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var result []conf.InputTransaction
	for rows.Next() {
		var tx conf.InputTransaction
		if err = rows.Scan(&tx.ID, &tx.AccountID, &tx.TxHash, &tx.Amount, &tx.BlockNumber, &tx.Date, &tx.Confirmations, &tx.IsNew); err != nil {
			return nil, err
		}

		result = append(result, tx)

		//Update isNew
		_, err = ConnectionPool.Exec("UPDATE input_transactions SET isNew = false WHERE id = $1", tx.ID)
		if err != nil {
			return nil, err
		}
	}

	return result, nil
}

func GetAccountIDByAddress(address string) (int, error) {
	row := ConnectionPool.QueryRow("SELECT id FROM accounts WHERE address = $1", address)
	var id int
	err := row.Scan(&id)

	return id, err
}

func GetAccountAddressByID(id int) (string, error) {
	row := ConnectionPool.QueryRow("SELECT address FROM accounts WHERE id = $1", id)
	var address string
	err := row.Scan(&address)

	return address, err
}
