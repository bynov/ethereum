package helper

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBlockHashToNumber(t *testing.T) {
	hash := "0x1A7F64"
	n, err := BlockHashToNumber(hash)
	assert.Empty(t, err)
	assert.Equal(t, 1736548, n)
}

func TestWeiToEth(t *testing.T) {
	wei := "0xb5e620f48000"

	res, err := WeiToEth(wei)
	assert.Empty(t, err)
	assert.Equal(t, 0.0002, res)
}

func TestEthToWei(t *testing.T) {
	eth := 0.0002

	gwei := EthToWei(eth)
	assert.Equal(t, "0xb5e620f48000", gwei)
}
