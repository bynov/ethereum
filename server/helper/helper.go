package helper

import (
	"log"
	"strconv"
)

const (
	hexPrefix     = "0x"
	weiMultiplier = 1000000000000000000
)

//EthToWei convert float64 eth to Wei hash string
func EthToWei(eth float64) string {
	return hexPrefix + strconv.FormatInt(int64(eth*weiMultiplier), 16)
}

//WeiToEth convert Wei hash string to float64 Eth
func WeiToEth(wei string) (float64, error) {
	wei = wei[len(hexPrefix):]
	weiInt64, err := strconv.ParseInt(wei, 16, 64)
	res := float64(weiInt64) / weiMultiplier
	return res, err
}

//BlockHashToNumber convert blockHash string to int block number
func BlockHashToNumber(hash string) (int, error) {
	hash = string(hash[len(hexPrefix):])
	log.Print(hash)

	i, err := strconv.ParseInt(hash, 16, 0)
	return int(i), err
}
