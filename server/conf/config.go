package conf

const (
	SendEthMethod = "SendEth"
	GetLastMethod = "GetLast"

	GasPrice = "0x3B9ACA00"
	Gas      = "0x26DCF"

	Endpoint = "http://geth:8545"
)
