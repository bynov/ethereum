package conf

type Account struct {
	ID      int
	Address string
	Balance string
}

type InputTransaction struct {
	ID            int
	AccountID     int
	TxHash        string
	BlockNumber   int
	Amount        string
	Confirmations int
	Date          int64
	IsNew         bool
}

type OutputTransaction struct {
	ID        int
	AccountID int
	TxHash    string
	To        string
	Amount    string
	Date      int64
}
